import Settings._
import Dependencies._

import scala.language.postfixOps
import scala.sys.process._

// scalastyle:off

name := "kafka-workshop"
version := "1.0-SNAPSHOT"

// =============================================================================
// Some custom task definitions to bootstrap the backends for local dev
// =============================================================================
lazy val startBackends =
  taskKey[Unit](
    "Start backend containers in docker/single-node/docker-compose.yml."
  )
lazy val stopBackends =
  taskKey[Unit](
    "Stop backend containers in docker/single-node/docker-compose.yml."
  )
lazy val restartBackends =
  taskKey[Unit](
    "Restart backend containers in docker/single-node/docker-compose.yml."
  )

startBackends := { "./docker/single-node/backends.sh start" ! }
stopBackends := { "./docker/single-node/backends.sh stop" ! }
restartBackends := { "./docker/single-node/backends.sh restart" ! }

lazy val root = (project in file("."))
  .settings(NoPublish)
  .settings(scalaVersion := Versions.scalaVersion)
  .aggregate(
    shared,
    producer,
    consumer,
    streamProcessor
  )

lazy val shared = BaseProject("shared")
  .settings(NoPublish)
  .settings(libraryDependencies += Kafka.kafkaClients)
  .settings(libraryDependencies ++= ConfigDeps.all ++ Circe.all)

lazy val producer = BaseProject("producer")
  .enablePlugins(JavaAppPackaging, DockerPlugin)
  .settings(NoPublish)
  .settings(dockerSettings())
  .settings(
    libraryDependencies ++= Seq(
      Akka.akkaSlf4j,
      Akka.akkaActor,
      Akka.akkaStream,
      Akka.akkaStreamKafka,
      Akka.alpakkaCsv,
      Kafka.kafka,
      Kafka.kafkaClients,
      Testing.scalaTest      % Test,
      Testing.scalaTestKafka % Test,
      Testing.scalactic
    ) ++ ConfigDeps.all ++ Circe.all ++ Logging.all
  )
  .dependsOn(shared)

lazy val consumer = BaseProject("consumer")
  .enablePlugins(JavaAppPackaging, DockerPlugin)
  .settings(NoPublish)
  .settings(dockerSettings())
  .settings(
    libraryDependencies ++= Seq(
      Akka.akkaSlf4j,
      Akka.akkaActor,
      Akka.akkaStream,
      Akka.akkaStreamKafka,
      Kafka.kafka,
      Kafka.kafkaClients,
      Testing.scalaTest      % Test,
      Testing.scalaTestKafka % Test,
      Testing.scalactic
    ) ++ ConfigDeps.all ++ Circe.all ++ Logging.all
  )
  .dependsOn(shared)

lazy val streamProcessor = BaseProject("stream-processor")
  .enablePlugins(JavaAppPackaging, DockerPlugin)
  .settings(NoPublish)
  .settings(dockerSettings())
  .settings(
    libraryDependencies ++= Seq(
      Kafka.kafka,
      Kafka.kafkaStreamsScala,
      Kafka.kafkaStreamsQuery,
      Testing.scalaTest      % Test,
      Testing.scalaTestKafka % Test,
      Testing.scalactic,
    ) ++ ConfigDeps.all ++ Circe.all ++ Logging.all
  )
  .dependsOn(shared)
