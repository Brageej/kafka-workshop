package net.scalytica.kafkaworkshop.shared.serdes

import java.util.{Map => JMap}

import io.circe.parser._
import io.circe.syntax._
import io.circe.{Decoder, Encoder}
import org.apache.kafka.common.serialization.{
  Deserializer,
  Serde,
  Serdes,
  Serializer
}

final class CirceSerde[A](implicit enc: Encoder[A], dec: Decoder[A])
    extends Serde[A] {
  def configure(configs: JMap[String, _], isKey: Boolean): Unit = {}

  def close(): Unit = {}

  override def serializer(): Serializer[A] = new CirceSerializer[A]

  override def deserializer(): Deserializer[A] = new CirceDeserializer[A]
}

final class CirceSerializer[A](implicit enc: Encoder[A]) extends Serializer[A] {

  private[this] val underlying = Serdes.String().serializer()

  def configure(configs: JMap[String, _], isKey: Boolean): Unit = {}

  def close(): Unit = {}

  override def serialize(topic: String, data: A) = {
    val js = data.asJson.noSpaces
    underlying.serialize(topic, js)
  }
}

final class CirceDeserializer[A](implicit dec: Decoder[A])
    extends Deserializer[A] {

  private[this] val underlying = Serdes.String().deserializer()

  def configure(configs: JMap[String, _], isKey: Boolean): Unit = {}

  def close(): Unit = {}

  override def deserialize(topic: String, data: Array[Byte]): A = {
    val jsStr = underlying.deserialize(topic, data)
    decode[A](jsStr) match {
      case Right(stats) => stats
      case Left(error)  => throw error
    }
  }
}
