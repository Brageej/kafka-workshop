package net.scalytica.kafkaworkshop
import com.typesafe.config.Config

package object shared {

  implicit class SafeConfig(val cfg: Config) {

    def asOptString(key: String): Option[String] = Option(cfg.getString(key))

    def asString(key: String)(default: => String): String =
      asOptString(key).getOrElse(default)

  }

}
